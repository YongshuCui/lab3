.data
var1: .word 3
.text
.globl main

main:
                lw $t1, var1($0)  # $t1(val)
                move $t0,$t1      #$t0(i)<- $t1
		li $a1, 100       #$a1 <- 100
		
loop:
		ble $a1, $t0 exit # if t1 == 100 exit loop
		addi $t1, $t1, 1  # body of the loop
		addi $t0, $t0, 1  # add 1 to t1
		j loop            # jump back to the top
exit:
                sw $t1, var1($0)
		li $v0, 1         #sysceall #1:print_int
                lw $a0, var1($0)
		syscall
                jr $ra