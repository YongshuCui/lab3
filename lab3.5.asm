.data  0x10000000

msg1: .asciiz "I'm far away"
msg2: .asciiz "I'm nearby"
msg3: .asciiz "Please enter an integer number: "

.text
.globl main

main:
            li $v0, 4               # system call for print_str
            la $a0, msg3            # address of string to print
            syscall        
            li $v0,5                # syscall #5: read_int
            syscall
            move $t0, $v0           # the value read is in $v0
            li $v0, 4               # system call for print_str
            la $a0, msg3            # address of string to print
            syscall        
            li $v0,5
            syscall
            move $t1, $v0
              
            beq $t0,$t1, Far        # branch if($t0==$t1)
            li $v0, 4               # system call for print_str           
            la $a0, msg2            # address of string of print
            syscall 
            jr $ra
                
Far:
            li $v0, 4               # syscall call for print_str
            la $a0, msg1            # address of string of print
            syscall 
            jr $ra
                