.data
my_array: .space 40       #reserve 40 bytes (10 words)
initial_value: .word 3

.text
.globl main
#there's no register for array index i, since we can calculate the address directly.


main:
                lw $t1, initial_value($0)     # $t1(j) <- intital_value
                la $t0, my_array              # $t0 <- start address of array
		addi $a1, $t0,40              # $a1 <- end address of aery
		
loop:
		ble $a1, $t0 exit             # if $t0 >= $a1, exit loop
		sw $t1,0($t0)                 # my_array[i]=j
                addi $t1, $t1, 1              # j++
		addi $t0, $t0, 4              # calculate address of next element
		j loop                        # jump back to the top
exit:
               
                jr $ra